/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package menu;

/**
 *
 * @author Duoc
 */
public class AgregarPersonaJFrame extends javax.swing.JFrame {

    /**
     * Creates new form AgregarPersonaJFrame
     */
    public AgregarPersonaJFrame() {
        initComponents();
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        btnOptSexo = new javax.swing.ButtonGroup();
        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        jLabel5 = new javax.swing.JLabel();
        txtNombre = new javax.swing.JTextField();
        txtApellido = new javax.swing.JTextField();
        txtRut = new javax.swing.JTextField();
        txtDireccion = new javax.swing.JTextField();
        btnAgregarPersona = new javax.swing.JButton();
        lblResultado = new javax.swing.JLabel();
        optHombre = new javax.swing.JRadioButton();
        optMujer = new javax.swing.JRadioButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setTitle("Agregar Persona");
        setMinimumSize(new java.awt.Dimension(400, 350));
        setResizable(false);
        getContentPane().setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        jLabel1.setText("Agregar Persona");
        getContentPane().add(jLabel1, new org.netbeans.lib.awtextra.AbsoluteConstraints(96, 18, -1, -1));

        jLabel2.setText("Nombre:");
        getContentPane().add(jLabel2, new org.netbeans.lib.awtextra.AbsoluteConstraints(40, 46, -1, -1));

        jLabel3.setText("Apellido: ");
        getContentPane().add(jLabel3, new org.netbeans.lib.awtextra.AbsoluteConstraints(40, 84, -1, -1));

        jLabel4.setText("Rut:");
        getContentPane().add(jLabel4, new org.netbeans.lib.awtextra.AbsoluteConstraints(40, 122, -1, -1));

        jLabel5.setText("Direccion:");
        getContentPane().add(jLabel5, new org.netbeans.lib.awtextra.AbsoluteConstraints(40, 160, -1, -1));

        txtNombre.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                txtNombreFocusGained(evt);
            }
        });
        getContentPane().add(txtNombre, new org.netbeans.lib.awtextra.AbsoluteConstraints(120, 40, 165, -1));
        getContentPane().add(txtApellido, new org.netbeans.lib.awtextra.AbsoluteConstraints(119, 81, 165, -1));
        getContentPane().add(txtRut, new org.netbeans.lib.awtextra.AbsoluteConstraints(119, 119, 165, -1));
        getContentPane().add(txtDireccion, new org.netbeans.lib.awtextra.AbsoluteConstraints(119, 157, 165, -1));

        btnAgregarPersona.setText("Agregar");
        btnAgregarPersona.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnAgregarPersonaActionPerformed(evt);
            }
        });
        getContentPane().add(btnAgregarPersona, new org.netbeans.lib.awtextra.AbsoluteConstraints(130, 250, -1, -1));
        getContentPane().add(lblResultado, new org.netbeans.lib.awtextra.AbsoluteConstraints(140, 240, -1, -1));

        btnOptSexo.add(optHombre);
        optHombre.setSelected(true);
        optHombre.setText("Hombre");
        getContentPane().add(optHombre, new org.netbeans.lib.awtextra.AbsoluteConstraints(100, 190, -1, -1));

        btnOptSexo.add(optMujer);
        optMujer.setText("Mujer");
        getContentPane().add(optMujer, new org.netbeans.lib.awtextra.AbsoluteConstraints(40, 190, -1, -1));

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void btnAgregarPersonaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnAgregarPersonaActionPerformed
        String sexo = "";
        
        if(optHombre.isSelected()){
            sexo = optHombre.getText();//"Hombre";
        }else if(optMujer.isSelected()){
            sexo = optMujer.getText();//"Mujer";
        }
        
        Persona per = new Persona(txtNombre.getText().trim(),
                txtApellido.getText().trim(),
                txtRut.getText().trim(),
                txtDireccion.getText().trim(),
                sexo);
        MantenedorPersonas.agregarPersona(per);
        txtNombre.setText("");
        txtApellido.setText("");
        txtRut.setText("");
        txtDireccion.setText("");
        lblResultado.setText("Datos Agregados");
    }//GEN-LAST:event_btnAgregarPersonaActionPerformed

    private void txtNombreFocusGained(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_txtNombreFocusGained
        lblResultado.setText("");
    }//GEN-LAST:event_txtNombreFocusGained

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnAgregarPersona;
    private javax.swing.ButtonGroup btnOptSexo;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel lblResultado;
    private javax.swing.JRadioButton optHombre;
    private javax.swing.JRadioButton optMujer;
    private javax.swing.JTextField txtApellido;
    private javax.swing.JTextField txtDireccion;
    private javax.swing.JTextField txtNombre;
    private javax.swing.JTextField txtRut;
    // End of variables declaration//GEN-END:variables
}
