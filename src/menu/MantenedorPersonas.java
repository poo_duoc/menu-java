/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package menu;

import java.util.ArrayList;

/**
 *
 * @author Duoc
 */
public class MantenedorPersonas {
    
    private static ArrayList<Persona> valores = new ArrayList<>();
    
    public static void agregarPersona(Persona persona){
        valores.add(persona);
    }
    
    public static int cantidadDePersonas(){
        return valores.size();
    }
    
    public static boolean modificarPersona(Persona persona){
        boolean modificaValor = false;
        for(int x = 0; x < valores.size(); x++){
            if(valores.get(x).getRut().equals(persona.getRut())){
                valores.get(x).setNombre(persona.getNombre());
                valores.get(x).setApellido(persona.getApellido());
                valores.get(x).setDireccion(persona.getDireccion());
                modificaValor = true;
                break;
            }
        }
        return modificaValor;
    }
    
    public static Persona buscarPersona(String rut){
        Persona retorno = null;
        for(Persona aux : valores){
           if(aux.getRut().equals(rut)){
               retorno = aux;
               break;
           } 
        }
        return retorno;        
    }
    
    public static boolean eliminarPersona(String rut){
        boolean eliminoRegistro = false;
        for(int x = 0; x < valores.size(); x++){
           if(valores.get(x).getRut().equals(rut)){
                valores.remove(x);
                eliminoRegistro = true;
                break;
            } 
        }
        return eliminoRegistro;
    }
    
    public static ArrayList<Persona> listarPersonas(){
        return valores;
    }
    
}
